#/bin/bash

// Script that translates the json-simple 1.1 java code into objective-c code.
// Benedikt Iltisberger - 2014-01-29

j2objc --build-closure -use-arc --no-package-directories --prefixes PrefixMapping.properties -sourcepath json_simple-1.1-all/src -d target json_simple-1.1-all/src/org/json/simple/ItemList.java json_simple-1.1-all/src/org/json/simple/JSONArray.java json_simple-1.1-all/src/org/json/simple/JSONAware.java json_simple-1.1-all/src/org/json/simple/JSONObject.java json_simple-1.1-all/src/org/json/simple/JSONStreamAware.java json_simple-1.1-all/src/org/json/simple/JSONValue.java json_simple-1.1-all/src/org/json/simple/parser/ContainerFactory.java json_simple-1.1-all/src/org/json/simple/parser/ContentHandler.java json_simple-1.1-all/src/org/json/simple/parser/JSONParser.java json_simple-1.1-all/src/org/json/simple/parser/ParseException.java json_simple-1.1-all/src/org/json/simple/parser/Yylex.java json_simple-1.1-all/src/org/json/simple/parser/Yytoken.java