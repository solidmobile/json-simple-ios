//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: json_simple-1.1-all/src/org/json/simple/parser/JSONParser.java
//
//  Created by ag on 07.08.14.
//

#include "IOSClass.h"
#include "java/io/IOException.h"
#include "java/io/Reader.h"
#include "java/io/StringReader.h"
#include "java/lang/Error.h"
#include "java/lang/Integer.h"
#include "java/lang/RuntimeException.h"
#include "java/util/LinkedList.h"
#include "java/util/List.h"
#include "java/util/Map.h"
#include "org/json/simple/JSONArray.h"
#include "org/json/simple/JSONObject.h"
#include "org/json/simple/parser/ContainerFactory.h"
#include "org/json/simple/parser/ContentHandler.h"
#include "org/json/simple/parser/JSONParser.h"
#include "org/json/simple/parser/ParseException.h"
#include "org/json/simple/parser/Yylex.h"
#include "org/json/simple/parser/Yytoken.h"

@implementation JSONParser

- (int)peekStatusWithJavaUtilLinkedList:(JavaUtilLinkedList *)statusStack {
  if ([((JavaUtilLinkedList *) nil_chk(statusStack)) size] == 0) return -1;
  JavaLangInteger *status = (JavaLangInteger *) check_class_cast([statusStack getFirst], [JavaLangInteger class]);
  return [((JavaLangInteger *) nil_chk(status)) intValue];
}

- (void)reset {
  token_ = nil;
  status_ = JSONParser_S_INIT;
  handlerStatusStack_ = nil;
}

- (void)resetWithJavaIoReader:(JavaIoReader *)inArg {
  [((Yylex *) nil_chk(lexer_)) yyresetWithJavaIoReader:inArg];
  [self reset];
}

- (int)getPosition {
  return [((Yylex *) nil_chk(lexer_)) getPosition];
}

- (id)parseWithNSString:(NSString *)s {
  return [self parseWithNSString:s withContainerFactory:(id<ContainerFactory>) check_protocol_cast(nil, @protocol(ContainerFactory))];
}

- (id)parseWithNSString:(NSString *)s
   withContainerFactory:(id<ContainerFactory>)containerFactory {
  JavaIoStringReader *in = [[JavaIoStringReader alloc] initWithNSString:s];
  @try {
    return [self parseWithJavaIoReader:in withContainerFactory:containerFactory];
  }
  @catch (JavaIoIOException *ie) {
    @throw [[ParseException alloc] initWithInt:-1 withInt:ParseException_ERROR_UNEXPECTED_EXCEPTION withId:ie];
  }
}

- (id)parseWithJavaIoReader:(JavaIoReader *)inArg {
  return [self parseWithJavaIoReader:inArg withContainerFactory:(id<ContainerFactory>) check_protocol_cast(nil, @protocol(ContainerFactory))];
}

- (id)parseWithJavaIoReader:(JavaIoReader *)inArg
       withContainerFactory:(id<ContainerFactory>)containerFactory {
  [self resetWithJavaIoReader:inArg];
  JavaUtilLinkedList *statusStack = [[JavaUtilLinkedList alloc] init];
  JavaUtilLinkedList *valueStack = [[JavaUtilLinkedList alloc] init];
  @try {
    do {
      [self nextToken];
      switch (status_) {
        case JSONParser_S_INIT:
        switch (((Yytoken *) nil_chk(token_))->type_) {
          case Yytoken_TYPE_VALUE:
          status_ = JSONParser_S_IN_FINISHED_VALUE;
          [statusStack addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
          [valueStack addFirstWithId:token_->value_];
          break;
          case Yytoken_TYPE_LEFT_BRACE:
          status_ = JSONParser_S_IN_OBJECT;
          [statusStack addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
          [valueStack addFirstWithId:[self createObjectContainerWithContainerFactory:containerFactory]];
          break;
          case Yytoken_TYPE_LEFT_SQUARE:
          status_ = JSONParser_S_IN_ARRAY;
          [statusStack addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
          [valueStack addFirstWithId:[self createArrayContainerWithContainerFactory:containerFactory]];
          break;
          default:
          status_ = JSONParser_S_IN_ERROR;
        }
        break;
        case JSONParser_S_IN_FINISHED_VALUE:
        if (((Yytoken *) nil_chk(token_))->type_ == Yytoken_TYPE_EOF) return [valueStack removeFirst];
        else @throw [[ParseException alloc] initWithInt:[self getPosition] withInt:ParseException_ERROR_UNEXPECTED_TOKEN withId:token_];
        case JSONParser_S_IN_OBJECT:
        switch (((Yytoken *) nil_chk(token_))->type_) {
          case Yytoken_TYPE_COMMA:
          break;
          case Yytoken_TYPE_VALUE:
          if ([token_->value_ isKindOfClass:[NSString class]]) {
            NSString *key = (NSString *) check_class_cast(token_->value_, [NSString class]);
            [valueStack addFirstWithId:key];
            status_ = JSONParser_S_PASSED_PAIR_KEY;
            [statusStack addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
          }
          else {
            status_ = JSONParser_S_IN_ERROR;
          }
          break;
          case Yytoken_TYPE_RIGHT_BRACE:
          if ([valueStack size] > 1) {
            (void) [statusStack removeFirst];
            (void) [valueStack removeFirst];
            status_ = [self peekStatusWithJavaUtilLinkedList:statusStack];
          }
          else {
            status_ = JSONParser_S_IN_FINISHED_VALUE;
          }
          break;
          default:
          status_ = JSONParser_S_IN_ERROR;
          break;
        }
        break;
        case JSONParser_S_PASSED_PAIR_KEY:
        {
          NSString *key;
          id<JavaUtilMap> parent;
          id<JavaUtilList> newArray;
          id<JavaUtilMap> newObject;
          switch (((Yytoken *) nil_chk(token_))->type_) {
            case Yytoken_TYPE_COLON:
            break;
            case Yytoken_TYPE_VALUE:
            (void) [statusStack removeFirst];
            key = (NSString *) check_class_cast([valueStack removeFirst], [NSString class]);
            parent = (id<JavaUtilMap>) check_protocol_cast([valueStack getFirst], @protocol(JavaUtilMap));
            (void) [((id<JavaUtilMap>) nil_chk(parent)) putWithId:key withId:token_->value_];
            status_ = [self peekStatusWithJavaUtilLinkedList:statusStack];
            break;
            case Yytoken_TYPE_LEFT_SQUARE:
            (void) [statusStack removeFirst];
            key = (NSString *) check_class_cast([valueStack removeFirst], [NSString class]);
            parent = (id<JavaUtilMap>) check_protocol_cast([valueStack getFirst], @protocol(JavaUtilMap));
            newArray = [self createArrayContainerWithContainerFactory:containerFactory];
            (void) [((id<JavaUtilMap>) nil_chk(parent)) putWithId:key withId:newArray];
            status_ = JSONParser_S_IN_ARRAY;
            [statusStack addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
            [valueStack addFirstWithId:newArray];
            break;
            case Yytoken_TYPE_LEFT_BRACE:
            (void) [statusStack removeFirst];
            key = (NSString *) check_class_cast([valueStack removeFirst], [NSString class]);
            parent = (id<JavaUtilMap>) check_protocol_cast([valueStack getFirst], @protocol(JavaUtilMap));
            newObject = [self createObjectContainerWithContainerFactory:containerFactory];
            (void) [((id<JavaUtilMap>) nil_chk(parent)) putWithId:key withId:newObject];
            status_ = JSONParser_S_IN_OBJECT;
            [statusStack addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
            [valueStack addFirstWithId:newObject];
            break;
            default:
            status_ = JSONParser_S_IN_ERROR;
          }
        }
        break;
        case JSONParser_S_IN_ARRAY:
        {
          id<JavaUtilList> val;
          id<JavaUtilMap> newObject;
          id<JavaUtilList> newArray;
          switch (((Yytoken *) nil_chk(token_))->type_) {
            case Yytoken_TYPE_COMMA:
            break;
            case Yytoken_TYPE_VALUE:
            val = (id<JavaUtilList>) check_protocol_cast([valueStack getFirst], @protocol(JavaUtilList));
            [((id<JavaUtilList>) nil_chk(val)) addWithId:token_->value_];
            break;
            case Yytoken_TYPE_RIGHT_SQUARE:
            if ([valueStack size] > 1) {
              (void) [statusStack removeFirst];
              (void) [valueStack removeFirst];
              status_ = [self peekStatusWithJavaUtilLinkedList:statusStack];
            }
            else {
              status_ = JSONParser_S_IN_FINISHED_VALUE;
            }
            break;
            case Yytoken_TYPE_LEFT_BRACE:
            val = (id<JavaUtilList>) check_protocol_cast([valueStack getFirst], @protocol(JavaUtilList));
            newObject = [self createObjectContainerWithContainerFactory:containerFactory];
            [((id<JavaUtilList>) nil_chk(val)) addWithId:newObject];
            status_ = JSONParser_S_IN_OBJECT;
            [statusStack addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
            [valueStack addFirstWithId:newObject];
            break;
            case Yytoken_TYPE_LEFT_SQUARE:
            val = (id<JavaUtilList>) check_protocol_cast([valueStack getFirst], @protocol(JavaUtilList));
            newArray = [self createArrayContainerWithContainerFactory:containerFactory];
            [((id<JavaUtilList>) nil_chk(val)) addWithId:newArray];
            status_ = JSONParser_S_IN_ARRAY;
            [statusStack addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
            [valueStack addFirstWithId:newArray];
            break;
            default:
            status_ = JSONParser_S_IN_ERROR;
          }
        }
        break;
        case JSONParser_S_IN_ERROR:
        @throw [[ParseException alloc] initWithInt:[self getPosition] withInt:ParseException_ERROR_UNEXPECTED_TOKEN withId:token_];
      }
      if (status_ == JSONParser_S_IN_ERROR) {
        @throw [[ParseException alloc] initWithInt:[self getPosition] withInt:ParseException_ERROR_UNEXPECTED_TOKEN withId:token_];
      }
    }
    while (((Yytoken *) nil_chk(token_))->type_ != Yytoken_TYPE_EOF);
  }
  @catch (JavaIoIOException *ie) {
    @throw ie;
  }
  @throw [[ParseException alloc] initWithInt:[self getPosition] withInt:ParseException_ERROR_UNEXPECTED_TOKEN withId:token_];
}

- (void)nextToken {
  token_ = [((Yylex *) nil_chk(lexer_)) yylex];
  if (token_ == nil) token_ = [[Yytoken alloc] initWithInt:Yytoken_TYPE_EOF withId:nil];
}

- (id<JavaUtilMap>)createObjectContainerWithContainerFactory:(id<ContainerFactory>)containerFactory {
  if (containerFactory == nil) return [[JSONObject alloc] init];
  id<JavaUtilMap> m = [((id<ContainerFactory>) nil_chk(containerFactory)) createObjectContainer];
  if (m == nil) return [[JSONObject alloc] init];
  return m;
}

- (id<JavaUtilList>)createArrayContainerWithContainerFactory:(id<ContainerFactory>)containerFactory {
  if (containerFactory == nil) return [[JSONArray alloc] init];
  id<JavaUtilList> l = [((id<ContainerFactory>) nil_chk(containerFactory)) creatArrayContainer];
  if (l == nil) return [[JSONArray alloc] init];
  return l;
}

- (void)parseWithNSString:(NSString *)s
       withContentHandler:(id<ContentHandler>)contentHandler {
  [self parseWithNSString:s withContentHandler:contentHandler withBoolean:NO];
}

- (void)parseWithNSString:(NSString *)s
       withContentHandler:(id<ContentHandler>)contentHandler
              withBoolean:(BOOL)isResume {
  JavaIoStringReader *in = [[JavaIoStringReader alloc] initWithNSString:s];
  @try {
    [self parseWithJavaIoReader:in withContentHandler:contentHandler withBoolean:isResume];
  }
  @catch (JavaIoIOException *ie) {
    @throw [[ParseException alloc] initWithInt:-1 withInt:ParseException_ERROR_UNEXPECTED_EXCEPTION withId:ie];
  }
}

- (void)parseWithJavaIoReader:(JavaIoReader *)inArg
           withContentHandler:(id<ContentHandler>)contentHandler {
  [self parseWithJavaIoReader:inArg withContentHandler:contentHandler withBoolean:NO];
}

- (void)parseWithJavaIoReader:(JavaIoReader *)inArg
           withContentHandler:(id<ContentHandler>)contentHandler
                  withBoolean:(BOOL)isResume {
  if (!isResume) {
    [self resetWithJavaIoReader:inArg];
    handlerStatusStack_ = [[JavaUtilLinkedList alloc] init];
  }
  else {
    if (handlerStatusStack_ == nil) {
      isResume = NO;
      [self resetWithJavaIoReader:inArg];
      handlerStatusStack_ = [[JavaUtilLinkedList alloc] init];
    }
  }
  JavaUtilLinkedList *statusStack = handlerStatusStack_;
  @try {
    do {
      switch (status_) {
        case JSONParser_S_INIT:
        [((id<ContentHandler>) nil_chk(contentHandler)) startJSON];
        [self nextToken];
        switch (((Yytoken *) nil_chk(token_))->type_) {
          case Yytoken_TYPE_VALUE:
          status_ = JSONParser_S_IN_FINISHED_VALUE;
          [((JavaUtilLinkedList *) nil_chk(statusStack)) addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
          if (![contentHandler primitiveWithId:token_->value_]) return;
          break;
          case Yytoken_TYPE_LEFT_BRACE:
          status_ = JSONParser_S_IN_OBJECT;
          [((JavaUtilLinkedList *) nil_chk(statusStack)) addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
          if (![contentHandler startObject]) return;
          break;
          case Yytoken_TYPE_LEFT_SQUARE:
          status_ = JSONParser_S_IN_ARRAY;
          [((JavaUtilLinkedList *) nil_chk(statusStack)) addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
          if (![contentHandler startArray]) return;
          break;
          default:
          status_ = JSONParser_S_IN_ERROR;
        }
        break;
        case JSONParser_S_IN_FINISHED_VALUE:
        [self nextToken];
        if (((Yytoken *) nil_chk(token_))->type_ == Yytoken_TYPE_EOF) {
          [((id<ContentHandler>) nil_chk(contentHandler)) endJSON];
          status_ = JSONParser_S_END;
          return;
        }
        else {
          status_ = JSONParser_S_IN_ERROR;
          @throw [[ParseException alloc] initWithInt:[self getPosition] withInt:ParseException_ERROR_UNEXPECTED_TOKEN withId:token_];
        }
        case JSONParser_S_IN_OBJECT:
        [self nextToken];
        switch (((Yytoken *) nil_chk(token_))->type_) {
          case Yytoken_TYPE_COMMA:
          break;
          case Yytoken_TYPE_VALUE:
          if ([token_->value_ isKindOfClass:[NSString class]]) {
            NSString *key = (NSString *) check_class_cast(token_->value_, [NSString class]);
            status_ = JSONParser_S_PASSED_PAIR_KEY;
            [((JavaUtilLinkedList *) nil_chk(statusStack)) addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
            if (![((id<ContentHandler>) nil_chk(contentHandler)) startObjectEntryWithNSString:key]) return;
          }
          else {
            status_ = JSONParser_S_IN_ERROR;
          }
          break;
          case Yytoken_TYPE_RIGHT_BRACE:
          if ([((JavaUtilLinkedList *) nil_chk(statusStack)) size] > 1) {
            (void) [statusStack removeFirst];
            status_ = [self peekStatusWithJavaUtilLinkedList:statusStack];
          }
          else {
            status_ = JSONParser_S_IN_FINISHED_VALUE;
          }
          if (![((id<ContentHandler>) nil_chk(contentHandler)) endObject]) return;
          break;
          default:
          status_ = JSONParser_S_IN_ERROR;
          break;
        }
        break;
        case JSONParser_S_PASSED_PAIR_KEY:
        [self nextToken];
        switch (((Yytoken *) nil_chk(token_))->type_) {
          case Yytoken_TYPE_COLON:
          break;
          case Yytoken_TYPE_VALUE:
          (void) [((JavaUtilLinkedList *) nil_chk(statusStack)) removeFirst];
          status_ = [self peekStatusWithJavaUtilLinkedList:statusStack];
          if (![((id<ContentHandler>) nil_chk(contentHandler)) primitiveWithId:token_->value_]) return;
          if (![contentHandler endObjectEntry]) return;
          break;
          case Yytoken_TYPE_LEFT_SQUARE:
          (void) [((JavaUtilLinkedList *) nil_chk(statusStack)) removeFirst];
          [statusStack addFirstWithId:[[JavaLangInteger alloc] initWithInt:JSONParser_S_IN_PAIR_VALUE]];
          status_ = JSONParser_S_IN_ARRAY;
          [statusStack addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
          if (![((id<ContentHandler>) nil_chk(contentHandler)) startArray]) return;
          break;
          case Yytoken_TYPE_LEFT_BRACE:
          (void) [((JavaUtilLinkedList *) nil_chk(statusStack)) removeFirst];
          [statusStack addFirstWithId:[[JavaLangInteger alloc] initWithInt:JSONParser_S_IN_PAIR_VALUE]];
          status_ = JSONParser_S_IN_OBJECT;
          [statusStack addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
          if (![((id<ContentHandler>) nil_chk(contentHandler)) startObject]) return;
          break;
          default:
          status_ = JSONParser_S_IN_ERROR;
        }
        break;
        case JSONParser_S_IN_PAIR_VALUE:
        (void) [((JavaUtilLinkedList *) nil_chk(statusStack)) removeFirst];
        status_ = [self peekStatusWithJavaUtilLinkedList:statusStack];
        if (![((id<ContentHandler>) nil_chk(contentHandler)) endObjectEntry]) return;
        break;
        case JSONParser_S_IN_ARRAY:
        [self nextToken];
        switch (((Yytoken *) nil_chk(token_))->type_) {
          case Yytoken_TYPE_COMMA:
          break;
          case Yytoken_TYPE_VALUE:
          if (![((id<ContentHandler>) nil_chk(contentHandler)) primitiveWithId:token_->value_]) return;
          break;
          case Yytoken_TYPE_RIGHT_SQUARE:
          if ([((JavaUtilLinkedList *) nil_chk(statusStack)) size] > 1) {
            (void) [statusStack removeFirst];
            status_ = [self peekStatusWithJavaUtilLinkedList:statusStack];
          }
          else {
            status_ = JSONParser_S_IN_FINISHED_VALUE;
          }
          if (![((id<ContentHandler>) nil_chk(contentHandler)) endArray]) return;
          break;
          case Yytoken_TYPE_LEFT_BRACE:
          status_ = JSONParser_S_IN_OBJECT;
          [((JavaUtilLinkedList *) nil_chk(statusStack)) addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
          if (![((id<ContentHandler>) nil_chk(contentHandler)) startObject]) return;
          break;
          case Yytoken_TYPE_LEFT_SQUARE:
          status_ = JSONParser_S_IN_ARRAY;
          [((JavaUtilLinkedList *) nil_chk(statusStack)) addFirstWithId:[[JavaLangInteger alloc] initWithInt:status_]];
          if (![((id<ContentHandler>) nil_chk(contentHandler)) startArray]) return;
          break;
          default:
          status_ = JSONParser_S_IN_ERROR;
        }
        break;
        case JSONParser_S_END:
        return;
        case JSONParser_S_IN_ERROR:
        @throw [[ParseException alloc] initWithInt:[self getPosition] withInt:ParseException_ERROR_UNEXPECTED_TOKEN withId:token_];
      }
      if (status_ == JSONParser_S_IN_ERROR) {
        @throw [[ParseException alloc] initWithInt:[self getPosition] withInt:ParseException_ERROR_UNEXPECTED_TOKEN withId:token_];
      }
    }
    while (((Yytoken *) nil_chk(token_))->type_ != Yytoken_TYPE_EOF);
  }
  @catch (JavaIoIOException *ie) {
    status_ = JSONParser_S_IN_ERROR;
    @throw ie;
  }
  @catch (ParseException *pe) {
    status_ = JSONParser_S_IN_ERROR;
    @throw pe;
  }
  @catch (JavaLangRuntimeException *re) {
    status_ = JSONParser_S_IN_ERROR;
    @throw re;
  }
  @catch (JavaLangError *e) {
    status_ = JSONParser_S_IN_ERROR;
    @throw e;
  }
  status_ = JSONParser_S_IN_ERROR;
  @throw [[ParseException alloc] initWithInt:[self getPosition] withInt:ParseException_ERROR_UNEXPECTED_TOKEN withId:token_];
}

- (id)init {
  if (self = [super init]) {
    lexer_ = [[Yylex alloc] initWithJavaIoReader:(JavaIoReader *) check_class_cast(nil, [JavaIoReader class])];
    token_ = nil;
    status_ = JSONParser_S_INIT;
  }
  return self;
}

- (void)copyAllFieldsTo:(JSONParser *)other {
  [super copyAllFieldsTo:other];
  other->handlerStatusStack_ = handlerStatusStack_;
  other->lexer_ = lexer_;
  other->status_ = status_;
  other->token_ = token_;
}

+ (J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { "peekStatusWithJavaUtilLinkedList:", "peekStatus", "I", 0x2, NULL },
    { "reset", NULL, "V", 0x1, NULL },
    { "resetWithJavaIoReader:", "reset", "V", 0x1, NULL },
    { "getPosition", NULL, "I", 0x1, NULL },
    { "parseWithNSString:", "parse", "Ljava.lang.Object;", 0x1, "Lorg.json.simple.parser.ParseException;" },
    { "parseWithNSString:withContainerFactory:", "parse", "Ljava.lang.Object;", 0x1, "Lorg.json.simple.parser.ParseException;" },
    { "parseWithJavaIoReader:", "parse", "Ljava.lang.Object;", 0x1, "Ljava.io.IOException;Lorg.json.simple.parser.ParseException;" },
    { "parseWithJavaIoReader:withContainerFactory:", "parse", "Ljava.lang.Object;", 0x1, "Ljava.io.IOException;Lorg.json.simple.parser.ParseException;" },
    { "nextToken", NULL, "V", 0x2, "Lorg.json.simple.parser.ParseException;Ljava.io.IOException;" },
    { "createObjectContainerWithContainerFactory:", "createObjectContainer", "Ljava.util.Map;", 0x2, NULL },
    { "createArrayContainerWithContainerFactory:", "createArrayContainer", "Ljava.util.List;", 0x2, NULL },
    { "parseWithNSString:withContentHandler:", "parse", "V", 0x1, "Lorg.json.simple.parser.ParseException;" },
    { "parseWithNSString:withContentHandler:withBoolean:", "parse", "V", 0x1, "Lorg.json.simple.parser.ParseException;" },
    { "parseWithJavaIoReader:withContentHandler:", "parse", "V", 0x1, "Ljava.io.IOException;Lorg.json.simple.parser.ParseException;" },
    { "parseWithJavaIoReader:withContentHandler:withBoolean:", "parse", "V", 0x1, "Ljava.io.IOException;Lorg.json.simple.parser.ParseException;" },
    { "init", NULL, NULL, 0x1, NULL },
  };
  static J2ObjcFieldInfo fields[] = {
    { "S_INIT_", NULL, 0x19, "I", NULL, .constantValue.asInt = JSONParser_S_INIT },
    { "S_IN_FINISHED_VALUE_", NULL, 0x19, "I", NULL, .constantValue.asInt = JSONParser_S_IN_FINISHED_VALUE },
    { "S_IN_OBJECT_", NULL, 0x19, "I", NULL, .constantValue.asInt = JSONParser_S_IN_OBJECT },
    { "S_IN_ARRAY_", NULL, 0x19, "I", NULL, .constantValue.asInt = JSONParser_S_IN_ARRAY },
    { "S_PASSED_PAIR_KEY_", NULL, 0x19, "I", NULL, .constantValue.asInt = JSONParser_S_PASSED_PAIR_KEY },
    { "S_IN_PAIR_VALUE_", NULL, 0x19, "I", NULL, .constantValue.asInt = JSONParser_S_IN_PAIR_VALUE },
    { "S_END_", NULL, 0x19, "I", NULL, .constantValue.asInt = JSONParser_S_END },
    { "S_IN_ERROR_", NULL, 0x19, "I", NULL, .constantValue.asInt = JSONParser_S_IN_ERROR },
    { "handlerStatusStack_", NULL, 0x2, "Ljava.util.LinkedList;", NULL,  },
    { "lexer_", NULL, 0x2, "Lorg.json.simple.parser.Yylex;", NULL,  },
    { "token_", NULL, 0x2, "Lorg.json.simple.parser.Yytoken;", NULL,  },
    { "status_", NULL, 0x2, "I", NULL,  },
  };
  static J2ObjcClassInfo _JSONParser = { "JSONParser", "org.json.simple.parser", NULL, 0x1, 16, methods, 12, fields, 0, NULL};
  return &_JSONParser;
}

@end
