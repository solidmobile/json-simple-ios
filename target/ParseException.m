//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: json_simple-1.1-all/src/org/json/simple/parser/ParseException.java
//
//  Created by ag on 07.08.14.
//

#include "java/lang/StringBuffer.h"
#include "org/json/simple/parser/ParseException.h"

@implementation ParseException

- (id)initWithInt:(int)errorType {
  return [self initParseExceptionWithInt:-1 withInt:errorType withId:nil];
}

- (id)initWithInt:(int)errorType
           withId:(id)unexpectedObject {
  return [self initParseExceptionWithInt:-1 withInt:errorType withId:unexpectedObject];
}

- (id)initParseExceptionWithInt:(int)position
                        withInt:(int)errorType
                         withId:(id)unexpectedObject {
  if (self = [super init]) {
    self->position_ = position;
    self->errorType_ = errorType;
    self->unexpectedObject_ = unexpectedObject;
  }
  return self;
}

- (id)initWithInt:(int)position
          withInt:(int)errorType
           withId:(id)unexpectedObject {
  return [self initParseExceptionWithInt:position withInt:errorType withId:unexpectedObject];
}

- (int)getErrorType {
  return errorType_;
}

- (void)setErrorTypeWithInt:(int)errorType {
  self->errorType_ = errorType;
}

- (int)getPosition {
  return position_;
}

- (void)setPositionWithInt:(int)position {
  self->position_ = position;
}

- (id)getUnexpectedObject {
  return unexpectedObject_;
}

- (void)setUnexpectedObjectWithId:(id)unexpectedObject {
  self->unexpectedObject_ = unexpectedObject;
}

- (NSString *)description {
  JavaLangStringBuffer *sb = [[JavaLangStringBuffer alloc] init];
  switch (errorType_) {
    case ParseException_ERROR_UNEXPECTED_CHAR:
    (void) [((JavaLangStringBuffer *) nil_chk([((JavaLangStringBuffer *) nil_chk([((JavaLangStringBuffer *) nil_chk([((JavaLangStringBuffer *) nil_chk([sb appendWithNSString:@"Unexpected character ("])) appendWithId:unexpectedObject_])) appendWithNSString:@") at position "])) appendWithInt:position_])) appendWithNSString:@"."];
    break;
    case ParseException_ERROR_UNEXPECTED_TOKEN:
    (void) [((JavaLangStringBuffer *) nil_chk([((JavaLangStringBuffer *) nil_chk([((JavaLangStringBuffer *) nil_chk([((JavaLangStringBuffer *) nil_chk([sb appendWithNSString:@"Unexpected token "])) appendWithId:unexpectedObject_])) appendWithNSString:@" at position "])) appendWithInt:position_])) appendWithNSString:@"."];
    break;
    case ParseException_ERROR_UNEXPECTED_EXCEPTION:
    (void) [((JavaLangStringBuffer *) nil_chk([((JavaLangStringBuffer *) nil_chk([((JavaLangStringBuffer *) nil_chk([sb appendWithNSString:@"Unexpected exception at position "])) appendWithInt:position_])) appendWithNSString:@": "])) appendWithId:unexpectedObject_];
    break;
    default:
    (void) [((JavaLangStringBuffer *) nil_chk([((JavaLangStringBuffer *) nil_chk([sb appendWithNSString:@"Unkown error at position "])) appendWithInt:position_])) appendWithNSString:@"."];
    break;
  }
  return [sb description];
}

- (void)copyAllFieldsTo:(ParseException *)other {
  [super copyAllFieldsTo:other];
  other->errorType_ = errorType_;
  other->position_ = position_;
  other->unexpectedObject_ = unexpectedObject_;
}

+ (J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { "initWithInt:", "ParseException", NULL, 0x1, NULL },
    { "initWithInt:withId:", "ParseException", NULL, 0x1, NULL },
    { "initWithInt:withInt:withId:", "ParseException", NULL, 0x1, NULL },
    { "getErrorType", NULL, "I", 0x1, NULL },
    { "setErrorTypeWithInt:", "setErrorType", "V", 0x1, NULL },
    { "getPosition", NULL, "I", 0x1, NULL },
    { "setPositionWithInt:", "setPosition", "V", 0x1, NULL },
    { "getUnexpectedObject", NULL, "Ljava.lang.Object;", 0x1, NULL },
    { "setUnexpectedObjectWithId:", "setUnexpectedObject", "V", 0x1, NULL },
    { "description", "toString", "Ljava.lang.String;", 0x1, NULL },
  };
  static J2ObjcFieldInfo fields[] = {
    { "serialVersionUID_ParseException_", "serialVersionUID", 0x1a, "J", NULL, .constantValue.asLong = ParseException_serialVersionUID },
    { "ERROR_UNEXPECTED_CHAR_", NULL, 0x19, "I", NULL, .constantValue.asInt = ParseException_ERROR_UNEXPECTED_CHAR },
    { "ERROR_UNEXPECTED_TOKEN_", NULL, 0x19, "I", NULL, .constantValue.asInt = ParseException_ERROR_UNEXPECTED_TOKEN },
    { "ERROR_UNEXPECTED_EXCEPTION_", NULL, 0x19, "I", NULL, .constantValue.asInt = ParseException_ERROR_UNEXPECTED_EXCEPTION },
    { "errorType_", NULL, 0x2, "I", NULL,  },
    { "unexpectedObject_", NULL, 0x2, "Ljava.lang.Object;", NULL,  },
    { "position_", NULL, 0x2, "I", NULL,  },
  };
  static J2ObjcClassInfo _ParseException = { "ParseException", "org.json.simple.parser", NULL, 0x1, 10, methods, 7, fields, 0, NULL};
  return &_ParseException;
}

@end
