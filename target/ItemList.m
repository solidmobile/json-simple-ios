//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: json_simple-1.1-all/src/org/json/simple/ItemList.java
//
//  Created by ag on 07.08.14.
//

#include "IOSObjectArray.h"
#include "java/lang/StringBuffer.h"
#include "java/util/ArrayList.h"
#include "java/util/List.h"
#include "java/util/StringTokenizer.h"
#include "org/json/simple/ItemList.h"

@implementation ItemList

- (id)init {
  if (self = [super init]) {
    sp_ = @",";
    items_ = [[JavaUtilArrayList alloc] init];
  }
  return self;
}

- (id)initWithNSString:(NSString *)s {
  if (self = [super init]) {
    sp_ = @",";
    items_ = [[JavaUtilArrayList alloc] init];
    [self splitWithNSString:s withNSString:sp_ withJavaUtilList:items_];
  }
  return self;
}

- (id)initWithNSString:(NSString *)s
          withNSString:(NSString *)sp {
  if (self = [super init]) {
    sp_ = @",";
    items_ = [[JavaUtilArrayList alloc] init];
    self->sp_ = s;
    [self splitWithNSString:s withNSString:sp withJavaUtilList:items_];
  }
  return self;
}

- (id)initWithNSString:(NSString *)s
          withNSString:(NSString *)sp
           withBoolean:(BOOL)isMultiToken {
  if (self = [super init]) {
    sp_ = @",";
    items_ = [[JavaUtilArrayList alloc] init];
    [self splitWithNSString:s withNSString:sp withJavaUtilList:items_ withBoolean:isMultiToken];
  }
  return self;
}

- (id<JavaUtilList>)getItems {
  return self->items_;
}

- (IOSObjectArray *)getArray {
  return (IOSObjectArray *) check_class_cast([((id<JavaUtilList>) nil_chk(self->items_)) toArray], [IOSObjectArray class]);
}

- (void)splitWithNSString:(NSString *)s
             withNSString:(NSString *)sp
         withJavaUtilList:(id<JavaUtilList>)append
              withBoolean:(BOOL)isMultiToken {
  if (s == nil || sp == nil) return;
  if (isMultiToken) {
    JavaUtilStringTokenizer *tokens = [[JavaUtilStringTokenizer alloc] initWithNSString:s withNSString:sp];
    while ([tokens hasMoreTokens]) {
      [((id<JavaUtilList>) nil_chk(append)) addWithId:[((NSString *) nil_chk([tokens nextToken])) trim]];
    }
  }
  else {
    [self splitWithNSString:s withNSString:sp withJavaUtilList:append];
  }
}

- (void)splitWithNSString:(NSString *)s
             withNSString:(NSString *)sp
         withJavaUtilList:(id<JavaUtilList>)append {
  if (s == nil || sp == nil) return;
  int pos = 0;
  int prevPos = 0;
  do {
    prevPos = pos;
    pos = [((NSString *) nil_chk(s)) indexOfString:sp fromIndex:pos];
    if (pos == -1) break;
    [((id<JavaUtilList>) nil_chk(append)) addWithId:[((NSString *) nil_chk([s substring:prevPos endIndex:pos])) trim]];
    pos += ((int) [((NSString *) nil_chk(sp)) length]);
  }
  while (pos != -1);
  [((id<JavaUtilList>) nil_chk(append)) addWithId:[((NSString *) nil_chk([((NSString *) nil_chk(s)) substring:prevPos])) trim]];
}

- (void)setSPWithNSString:(NSString *)sp {
  self->sp_ = sp;
}

- (void)addWithInt:(int)i
      withNSString:(NSString *)item {
  if (item == nil) return;
  [((id<JavaUtilList>) nil_chk(items_)) addWithInt:i withId:[((NSString *) nil_chk(item)) trim]];
}

- (void)addWithNSString:(NSString *)item {
  if (item == nil) return;
  [((id<JavaUtilList>) nil_chk(items_)) addWithId:[((NSString *) nil_chk(item)) trim]];
}

- (void)addAllWithItemList:(ItemList *)list {
  [((id<JavaUtilList>) nil_chk(items_)) addAllWithJavaUtilCollection:((ItemList *) nil_chk(list))->items_];
}

- (void)addAllWithNSString:(NSString *)s {
  [self splitWithNSString:s withNSString:sp_ withJavaUtilList:items_];
}

- (void)addAllWithNSString:(NSString *)s
              withNSString:(NSString *)sp {
  [self splitWithNSString:s withNSString:sp withJavaUtilList:items_];
}

- (void)addAllWithNSString:(NSString *)s
              withNSString:(NSString *)sp
               withBoolean:(BOOL)isMultiToken {
  [self splitWithNSString:s withNSString:sp withJavaUtilList:items_ withBoolean:isMultiToken];
}

- (NSString *)getWithInt:(int)i {
  return (NSString *) check_class_cast([((id<JavaUtilList>) nil_chk(items_)) getWithInt:i], [NSString class]);
}

- (int)size {
  return [((id<JavaUtilList>) nil_chk(items_)) size];
}

- (NSString *)description {
  return [self toStringWithNSString:sp_];
}

- (NSString *)toStringWithNSString:(NSString *)sp {
  JavaLangStringBuffer *sb = [[JavaLangStringBuffer alloc] init];
  for (int i = 0; i < [((id<JavaUtilList>) nil_chk(items_)) size]; i++) {
    if (i == 0) (void) [sb appendWithId:[items_ getWithInt:i]];
    else {
      (void) [sb appendWithNSString:sp];
      (void) [sb appendWithId:[items_ getWithInt:i]];
    }
  }
  return [sb description];
}

- (void)clear {
  [((id<JavaUtilList>) nil_chk(items_)) clear];
}

- (void)reset {
  sp_ = @",";
  [((id<JavaUtilList>) nil_chk(items_)) clear];
}

- (void)copyAllFieldsTo:(ItemList *)other {
  [super copyAllFieldsTo:other];
  other->items_ = items_;
  other->sp_ = sp_;
}

+ (J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { "init", "ItemList", NULL, 0x1, NULL },
    { "initWithNSString:", "ItemList", NULL, 0x1, NULL },
    { "initWithNSString:withNSString:", "ItemList", NULL, 0x1, NULL },
    { "initWithNSString:withNSString:withBoolean:", "ItemList", NULL, 0x1, NULL },
    { "getItems", NULL, "Ljava.util.List;", 0x1, NULL },
    { "getArray", NULL, "[Ljava.lang.String;", 0x1, NULL },
    { "splitWithNSString:withNSString:withJavaUtilList:withBoolean:", "split", "V", 0x1, NULL },
    { "splitWithNSString:withNSString:withJavaUtilList:", "split", "V", 0x1, NULL },
    { "setSPWithNSString:", "setSP", "V", 0x1, NULL },
    { "addWithInt:withNSString:", "add", "V", 0x1, NULL },
    { "addWithNSString:", "add", "V", 0x1, NULL },
    { "addAllWithItemList:", "addAll", "V", 0x1, NULL },
    { "addAllWithNSString:", "addAll", "V", 0x1, NULL },
    { "addAllWithNSString:withNSString:", "addAll", "V", 0x1, NULL },
    { "addAllWithNSString:withNSString:withBoolean:", "addAll", "V", 0x1, NULL },
    { "getWithInt:", "get", "Ljava.lang.String;", 0x1, NULL },
    { "size", NULL, "I", 0x1, NULL },
    { "description", "toString", "Ljava.lang.String;", 0x1, NULL },
    { "toStringWithNSString:", "toString", "Ljava.lang.String;", 0x1, NULL },
    { "clear", NULL, "V", 0x1, NULL },
    { "reset", NULL, "V", 0x1, NULL },
  };
  static J2ObjcFieldInfo fields[] = {
    { "sp_", NULL, 0x2, "Ljava.lang.String;", NULL,  },
    { "items_", NULL, 0x0, "Ljava.util.List;", NULL,  },
  };
  static J2ObjcClassInfo _ItemList = { "ItemList", "org.json.simple", NULL, 0x1, 21, methods, 2, fields, 0, NULL};
  return &_ItemList;
}

@end
